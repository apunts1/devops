# Playbooks

Els ansible playbooks son un conjunto d'arxius que faciliten l'administració de tasques en l'entorn ansible. Ens permetar crear un sistema de control de versions. És molt facil convertir shells scripts en playbooks.

de que és composa un playbook:
  - És un arxiu escrit en [YAML](apunts1.github.io/serialitzacio), per facilitarne la lectura.
  - un _playbook_ és un conjunt de _plays_, un _play_ té com objetiu mapejar un grup de hosts amb uns determinalts rols, i son respresentats per tasques. Les tasques són simplement crides a moduls Ansinble
  - Permet agrupar grans conjunts de maquines gràcies a l'estructura d'inventoris.
