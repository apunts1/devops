# DevOps

> Sentiu-vos lliures de fer una pull request amb qualsevol millora. Ja sigui en el contingut, afegint dades, o fins i tot correccions ortogràfiques.

---
**Repository:** [https://gitlab.com/apunts1/devops](https://gitlab.com/apunts1/devops)

---


Aquest repositori s'engloba dins d'un [conjunt de repositoris](https://gitlab.com/apunts1) que he creat per anar fent anotacions de tot allò que vaig aprenent en el món del software. La idea és utilitzar el repositori com a banc de proves amb exemples i aquesta documentació com a font d'informació per al meu dia a dia com a programador.

## Què és DevOps?


## Eines
- **[Ansible](/ansible):** Eina d'automatització de tasques en el món de IT
